<p style="font-size:2em; text-align:center; height:1.2em">
    Advanced Computer Architecture Assignment #1
</p>

<p style="font-size: 1.5em; text-align:center">
    Conway’s Game of Life with POSIX Threads
</p>

<p style="text-align:right">
    사이버보안학과 201720692
    <br>
    이시훈
</p>


# Build

```
$ make                              
g++ -o glife glife.cpp -pthread
$ ./glife ~~
```



# Parallelization

## Split

```c++

struct Work {
	int from, to;
};

pthread_barrier_t barrier;

#define NUM_THREADS (nprocs + 1)
```

먼저 worker thread가 인자로 사용할 구조체를 만들어 주었다. 큰 grid를 쪼개서 worker에 분배한다. task마다 처리할 grid의  범위를 지정해 주기 위해 `from`, `to`를 멤버로 두었다.

```c++

m_Grid[0] = (int*)malloc(sizeof(int) * (cols*rows));
if (m_Grid[0] == NULL) 
    cout << "3 Memory allocation error " << endl;

m_Temp[0] = (int*)malloc(sizeof(int) * (cols*rows));	
if (m_Temp[0] == NULL) 
    cout << "4 Memory allocation error " << endl;

for (int i=1; i< cols; i++) {
	m_Grid[i] = m_Grid[i-1] + rows;
	m_Temp[i] = m_Temp[i-1] + rows;
}
```

skeleton 코드에서 친절히 `cols*rows`로  contiguous 하게 메모리가 할당되어 있기 때문에 column과 row 별로 범위를 갖지 않아도 범위 한 개로 구간지정이 가능했다.

```c++
Work works[NUM_THREADS];
//set work range
int grid_size = g_GameOfLifeGrid->getCols() * g_GameOfLifeGrid->getRows();
int work_size = grid_size / NUM_THREADS;

for (int i = 0; i < NUM_THREADS; i++) {
	works[i].from = i * work_size;
	if (i == NUM_THREADS - 1)
		works[i].to = grid_size;
	else
		works[i].to = (i + 1) * work_size;
}
```

main 함수를 보면 threads 개수만큼 인자를 선언하고 grid의 크기(`grid_size`)를 쓰레드의 개수로 나누어 워커가 처리할 크기(`work_size`)를 구하였다. 쓰레드 개수만큼 반복을 돌며 워커의 범위를  지정해 주었다. 마지막 워커는 나머지가 생길수 있으므로 `grid_size` 를 `to`에 넣어 끝까지 수행하도록 한다 

ex) 10*10 에 쓰레드가  5개면

```
worker 1 : 0~19
worker 2 : 20~39
worker 3 : 40~59
worker 4 : 60~79
worker 5 : 80~99
```

10*10에 쓰레드가 6개면 

```
worker 1 : 0~15
worker 2 : 16~31
worker 3 : 32~47
worker 4 : 48~63
worker 5 : 64~79
worker 6 : 80~99 (+19)
```

의 범위를 각 워커 쓰레드가 처리한다.



## Game

게임에서의 병렬화 부분이다.

```c++
void GameOfLifeGrid::next(const int from, const int to)
{
	for (int i = from; i < to; ++i) {
		int x = i % m_Rows;
		int y = i / m_Rows;
		int neighbors = getNumOfNeighbors(x, y);
		bool lives = neighbors == 2 || neighbors == 3;
		bool dies = neighbors == 3;
		int next = isLive(x, y) ? lives : dies;
		next ? live(x, y) : dead(x, y);
	}
}
```

`next(const int from, const int to)` 이 함수는 범위를 인자로 받는다. `for (int i = from; i < to; ++i)` 이 루프는 `from`에서 `to` 까지 도는데 `getNumOfNeighbors()`로 해당 좌표에 인접한 살아있는 이웃의 개수를 구한다. conway 규칙에 따라 `islive()`로 선택한 셀이 살아있는지 확인하고 `live()` `dead()`로 `m_Temp`라는 임시 공간에 다음 세대 셀을 업데이트 한다.

```c++
int GameOfLifeGrid::getNumOfNeighbors(int cols, int rows)
{
	int neighbors = 0;
	for (int y_offset = -1; y_offset <= 1; ++y_offset) {
		for (int x_offset = -1; x_offset <= 1; ++x_offset) {
			if (x_offset == 0 && y_offset == 0) continue;
			int neighbor_x = (cols + x_offset + m_Cols) % m_Cols; // mitigate oob
			int neighbor_y = (rows + y_offset + m_Rows) % m_Rows;
			neighbors += isLive(neighbor_x, neighbor_y);
		}
	}
	return neighbors;
}
```

이 함수는 y축을 outer loop x축을 inner loop로 한다. cache의 locality 를 활용하기 위해 x축을 먼저 iteration하였다.

## pthread and barrier

```c++

// create threads
pthread_barrier_init(&barrier, NULL, NUM_THREADS + 1);
pthread_t threads[NUM_THREADS];
for (int i = 0; i < NUM_THREADS; i++)
	pthread_create(&threads[i], NULL, workerThread, (void*)&works[i]);

//invoke worker
for (int gen = 0; gen < g_GameOfLifeGrid->getGens(); gen++)
	g_GameOfLifeGrid->next();

pthread_barrier_destroy(&barrier);
```

쓰레드를 생성하면서 초기화 하는 부분이다. 맨 처음 `pthread_barrier_init()` 로 베리어를 쓰레드 + 1개로 count를 초기화 한다. +1인 이유는 베리어는 일반적으로 기다리는 메인쓰레드가 wait하기 때문에 쓰레드 개수보다 1개 많게 설정하였다.

`pthread_create()` 로 쓰레드를 생성하였고 `works[i]`로 범위를 전달 하였다.

`getGens()`만큼 루프를 돌면서 모든 세대를 시뮬레이션(`next()`)한다. 모두 끝나면 `pthread_barrier_destroy()`로 베리어를 해제한다. 

```c++
void GameOfLifeGrid::next()
{	
	pthread_barrier_wait(&barrier); //start barrier
	pthread_barrier_wait(&barrier); //end barrier
	// swap temp <-> grid
	auto temp = m_Grid;
	m_Grid = m_Temp;
	m_Temp = temp;
}
```

```c++
void* workerThread(void *arg)
{
	Work *work = (Work*)arg;
	for (int gen = 0; gen < g_GameOfLifeGrid->getGens(); gen++)
	{
		pthread_barrier_wait(&barrier); //start barrier
		g_GameOfLifeGrid->next(work->from, work->to);	
		pthread_barrier_wait(&barrier); //end barrier
	}
}
```

`pthread_barrier_wait()` 는 베리어를 기다리는 함수 이다. 여기서 베리어를 2번 기다린다. 한 wait은 각 worker가 모두 준비 상태가 될 때 까지 기다리는 것이고 두번째 wait은 모든 worker가 작업을 모두 마쳤는지 기다린다.  베리어는 설정한 카운트에 도달했을때 카운트를 초기화하여 다시 사용할 수 있다. 첫 wait을 지나면 카운트가 초기화 되고 두번째 wait을 지나면 카운트가 또 다시 초기화 된다. 

 temp 포인터로  `m_Grid` 와 `m_Temp`를 swap한다. `workerThread`에서 워커들이 업데이트한 임시 grid를 실제 grid에 반영하기 위한 작업이다. 임시 그리드를 설정할때 기존 값에 영향을 받지않고 모든 셀을 세팅하기 때문에 이전 값으로부터 영향이 없어 스왑해도 문제가 없다.

 

# Result

### puf-qb-c3_4290_258

가장 먼저 cell이 많은 `puf-qb-c3_4290_258` 로 테스트 하였다.

#### 10000 gens, 1000*1000 grid

##### single thread

```
st201720692@swarmx:~/pa1$ ./glife sample_inputs/puf-qb-c3_4290_258 1 10000 1000 1000
Execution Time: 477s
:: Dump X-Y coordinate
2 259
2 260
...
998 104
Program end...
```

477초가 걸렸고 

##### multi thread

```
st201720692@swarmx:~/pa1$ ./glife sample_inputs/puf-qb-c3_4290_258 128 10000 1000 1000
Execution Time: 46s
```

46초가 걸렸다. 시간은 9.64%로 줄었고 속도가 약 10배 빠르다.

#### 5000 gens, 500*500 grid

##### single thread

```
st201720692@swarmx:~/pa1$ ./glife sample_inputs/puf-qb-c3_4290_258 1 5000 500 500
Execution Time: 71s
:: Dump X-Y coordinate
0 28
...
499 56
Program end...
```

싱글 쓰레드는 71초가 소요됐고

##### multi thread

```
st201720692@swarmx:~/pa1$ ./glife sample_inputs/puf-qb-c3_4290_258 128 5000 500 500
Execution Time: 9s
```

멀티 쓰레드는 9초로 줄었다. 시간은 12.67%로 줄었고 속도는 788.9%로 약 8배 빨라졌다.



#### 23334m_4505_1008

이 파일은 셀이 가장 적다. 

#### single thread

```
st201720692@swarmx:~/pa1$ ./glife sample_inputs/23334m_4505_1008 1 5000 1000 1000
Execution Time: 241s
:: Dump X-Y coordinate
0 12
0 14
...
999 14
Program end...
```

싱글쓰레드는 241초가 걸렸고

##### multi thread

```
st201720692@swarmx:~/pa1$ ./glife sample_inputs/23334m_4505_1008 128 5000 1000 1000
Execution Time: 23s
```

멀티쓰레드는 23초로 비슷하게 약 10배가 빨라졌다.

#### make-a_71_81 128

샘플 개수는 95개로 `23334m_4505_1008` 보다 10배 많다.

#### single thread

```
st201720692@swarmx:~/pa1$ ./glife sample_inputs/make-a_71_81 1 5000 1000 1000
Execution Time: 233s
:: Dump X-Y coordinate
582 29
582 41
...
591 38
Program end...
```

싱글은 233초가 걸렸고

##### multi thread

```
st201720692@swarmx:~/pa1$ ./glife sample_inputs/make-a_71_81 128 5000 1000 1000
Execution Time: 23s
```

멀티쓰레드는 23초가 걸려 비슷하게 10배가 빨라졌다.

## Graph

`puf-qb-c3_4290_258` 파일에 쓰레드 개수를 x로 하고 10000세대 1000*1000 그리드를 기준으로 코어를 +32개씩 늘려보았다. 

| threads(x) | time |
| ---------- | ---- |
| 1          | 793s |
| 2          | 477s |
| 32         | 56s  |
| 63         | 47s  |
| 95         | 43s  |
| 128        | 42s  |
| 160        | 48s  |
| 192        | 48s  |
| 224        | 47s  |
| 256        | 50s  |

![image-20191216232409420](report.assets/image-20191216232409420.png)

전체 그래프를 표현해보았다. 800초-500초-50초대로 급격하게 속도가 향상된것을 확인할 수 있었다.

![image-20191216232558798](report.assets/image-20191216232558798.png)

차이가 너무 많이나 32쓰레드 부터 그래프를 표현해 보았을때 이런 형태가 나타났다. 128개 쓰레드일때 최적의 시간에 도달했고 그 이후부터는 elapsed time이 증가한 것을 확인 할 수 있다. 쓰레드 컨텍스트 스위칭 오버헤드나 베리어 사용으로 인한 오버헤드로 추정된다. 미세한 차이는 여러 사람이 시스템을 사용하여 사용한 리소스의 차이로 보인다.

급격한 차이를 보인 1~32쓰레드에서 세분화하여 테스트하지 못한 것이 아쉽다.



# References

* https://github.com/k-vekos/GameOfLife/blob/e9a1b822dc24bf336b5868bdaf3c127c455a6ba6/src/GameOfLife.cpp#L67
* <https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life>

* <https://docs.oracle.com/cd/E19253-01/816-5137/gfwek/index.html>
* <http://www.qnx.com/developers/docs/qnxcar2/index.jsp?topic=%2Fcom.qnx.doc.neutrino.getting_started%2Ftopic%2Fs1_procs_barriers.html> 