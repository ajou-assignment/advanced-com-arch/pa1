#include <iostream>
#include <fstream>
#include <pthread.h>
#include <stdlib.h>
#include <sys/time.h>

#define LIVE 1
#define DEAD 0

# define timersub(a, b, result)                 \
	do {                        \
		(result)->tv_sec = (a)->tv_sec - (b)->tv_sec;           \
		(result)->tv_usec = (a)->tv_usec - (b)->tv_usec;            \
		if ((result)->tv_usec < 0) {                \
			--(result)->tv_sec;                 \
			(result)->tv_usec += 1000000;               \
		}                       \
	} while (0)

using namespace std;

// function prototype
void* workerThread(void *);
void para_range(int, int, int, int, int*, int*);

struct Work {
	int from, to;
};

pthread_barrier_t barrier;

#define NUM_THREADS (nprocs + 1)
// 
class GameOfLifeGrid {

public:
	GameOfLifeGrid(int cols, int rows, int gen);
	
	void next();
	void next(const int from, const int to);
	
	int isLive(int cols, int rows) { return (m_Grid[cols][rows] ? LIVE: DEAD); }
	int getNumOfNeighbors(int cols, int rows);
	
	void dead(int cols, int rows) { m_Temp[cols][rows] = 0; }
	void live(int cols, int rows) { m_Temp[cols][rows] = 1; }

	int decGen() { return m_Generations--; }
	void setGen(int n) { m_Generations = n; }
	void setCell(int cols, int rows) { m_Grid[cols][rows] = true; }
	
	void dump();
	void dumpCoordinate();
	int* getColAddr(int col) { return m_Grid[col]; }

	int getCols() { return m_Cols; }
	int getRows() { return m_Rows; }
	int getGens() { return m_Generations; }

private:
	int** m_Grid;
	int** m_Temp;
	int m_Rows;
	int m_Cols;
	int m_Generations;

};

GameOfLifeGrid* g_GameOfLifeGrid;
int nprocs;

// Entry point
int main(int argc, char* argv[])
{
	int cols, rows, gen;
	ifstream inputFile;
	int x, y;
	struct timeval start_time, end_time, result_time;
	pthread_t* threadID;
	int status;
	#ifdef DEBUG
		argc = 6;
		char *debug_argv[6][512] = {{"glife", "sample_inputs/23334m_4505_1008", "4", "10000", "600", "600"},
							{"glife", "sample_inputs/make-a_71_81", "10000", "4", "100", "100"},
							{"glife", "sample_inputs/puf-qb-c3_4290_258", "4", "10000", "300", "300"},
							{"glife", "sample_inputs/ex", "4", "5", "100", "100"}};
		argv = debug_argv[2];
	#endif 

	if (argc != 6) {
		cout <<"Usage: " << argv[0] << " <input file> <nprocs> <# of generations> <width> <heigh>" << endl;
		return 1;
	}

	inputFile.open(argv[1], ifstream::in);

	if (inputFile.is_open() == false) {
		cout << "The "<< argv[1] << " file can not be opend" << endl;
		return 1;
	}
	
	//
	nprocs = atoi(argv[2]);
	gen = atoi(argv[3]);
	cols = atoi(argv[4]);
	rows = atoi(argv[5]);

	g_GameOfLifeGrid = new GameOfLifeGrid(cols, rows, gen);

	while (inputFile.good()) {
		inputFile >> x >> y;
		g_GameOfLifeGrid->setCell(x, y);
	}

	gettimeofday(&start_time, NULL);

    // HINT: YOU MAY NEED TO WRITE PTHREAD INVOKING CODES HERE
	Work works[NUM_THREADS];
	//set work range
	int grid_size = g_GameOfLifeGrid->getCols() * g_GameOfLifeGrid->getRows();
	int work_size = grid_size / NUM_THREADS;

	for (int i = 0; i < NUM_THREADS; i++) {
		works[i].from = i * work_size;
		if (i == NUM_THREADS - 1)
			works[i].to = grid_size;
		else
			works[i].to = (i + 1) * work_size;
	}

	// create threads
	pthread_barrier_init(&barrier, NULL, NUM_THREADS + 1);
	pthread_t threads[NUM_THREADS];
	for (int i = 0; i < NUM_THREADS; i++)
		pthread_create(&threads[i], NULL, workerThread, (void*)&works[i]);

	//invoke worker
	for (int gen = 0; gen < g_GameOfLifeGrid->getGens(); gen++)
		g_GameOfLifeGrid->next();
	
	pthread_barrier_destroy(&barrier);

	#ifdef DEBUG
		g_GameOfLifeGrid->dump();
	#endif
	gettimeofday(&end_time, NULL);
	timersub(&end_time, &start_time, &result_time);
	
	cout << "Execution Time: " << result_time.tv_sec <<  "s" << endl;
	g_GameOfLifeGrid->dumpCoordinate();

	inputFile.close();

	cout << "Program end... " << endl;
	return 0;
}

// HINT: YOU MAY NEED TO FILL OUT BELOW FUNCTIONS
void* workerThread(void *arg)
{
	Work *work = (Work*)arg;
	for (int gen = 0; gen < g_GameOfLifeGrid->getGens(); gen++)
	{
		pthread_barrier_wait(&barrier); //start barrier
		g_GameOfLifeGrid->next(work->from, work->to);	
		pthread_barrier_wait(&barrier); //end barrier
	}
}

void GameOfLifeGrid::next(const int from, const int to)
{
	for (int i = from; i < to; ++i) {
		int x = i % m_Rows;
		int y = i / m_Rows;
		int neighbors = getNumOfNeighbors(x, y);
		bool lives = neighbors == 2 || neighbors == 3;
		bool dies = neighbors == 3;
		int next = isLive(x, y) ? lives : dies;
		next ? live(x, y) : dead(x, y);
	}
}

void GameOfLifeGrid::next()
{	
	pthread_barrier_wait(&barrier); //start barrier
	pthread_barrier_wait(&barrier); //end barrier
	// swap temp <-> grid
	auto temp = m_Grid;
	m_Grid = m_Temp;
	m_Temp = temp;
}

int GameOfLifeGrid::getNumOfNeighbors(int cols, int rows)
{
	int neighbors = 0;
	for (int y_offset = -1; y_offset <= 1; ++y_offset) {
		for (int x_offset = -1; x_offset <= 1; ++x_offset) {
			if (x_offset == 0 && y_offset == 0) continue;
			int neighbor_x = (cols + x_offset + m_Cols) % m_Cols; // mitigate oob
			int neighbor_y = (rows + y_offset + m_Rows) % m_Rows;
			neighbors += isLive(neighbor_x, neighbor_y);
		}
	}
	return neighbors;
}

// HINT: YOU CAN MODIFY BELOW CODES IF YOU WANT
void GameOfLifeGrid::dump() 
{
	cout << "===============================" << endl;

	for (int i=0; i < m_Cols; i++) {
		
		cout << "[" << i << "] ";
		
		for (int j=0; j < m_Rows; j++) {
			cout << m_Grid[i][j] << ' ';
		}
		
		cout << endl;
	}
	
	cout << "===============================\n" << endl;
}

void GameOfLifeGrid::dumpCoordinate()
{
	cout << ":: Dump X-Y coordinate" << endl;

	for (int i=0; i < m_Cols; i++) {

		for (int j=0; j < m_Rows; j++) {

			if (m_Grid[i][j]) cout << i << " " << j << endl;
		}
	}
}


GameOfLifeGrid::GameOfLifeGrid(int cols, int rows, int gen)
{
	m_Generations = gen;
	m_Cols = cols;
	m_Rows = rows;

	m_Grid = (int**)malloc(sizeof(int*) * cols);

	if (m_Grid == NULL) 
		cout << "1 Memory allocation error " << endl;

	m_Temp = (int**)malloc(sizeof(int*) * cols);
	if (m_Temp == NULL) 
		cout << "2 Memory allocation error " << endl;


	m_Grid[0] = (int*)malloc(sizeof(int) * (cols*rows));
	if (m_Grid[0] == NULL) 
		cout << "3 Memory allocation error " << endl;

	m_Temp[0] = (int*)malloc(sizeof(int) * (cols*rows));	
	if (m_Temp[0] == NULL) 
		cout << "4 Memory allocation error " << endl;


	for (int i=1; i< cols; i++) {
		m_Grid[i] = m_Grid[i-1] + rows;
		m_Temp[i] = m_Temp[i-1] + rows;
	}

	for (int i=0; i < cols; i++) {
		for (int j=0; j < rows; j++) {
			m_Grid[i][j] = m_Temp[i][j] = 0;
		}
	}

}

